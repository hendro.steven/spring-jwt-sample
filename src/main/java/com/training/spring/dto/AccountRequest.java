package com.training.spring.dto;

import java.util.HashSet;
import java.util.Set;

import com.training.spring.models.entities.AccountRole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountRequest {

    private String fullName;

    private String email;

    private String password;

    private Set<AccountRole> roles = new HashSet<>();
}
