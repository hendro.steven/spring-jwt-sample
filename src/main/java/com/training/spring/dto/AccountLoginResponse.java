package com.training.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountLoginResponse {
    private String accessToken;
    private String token; // refreshToken
    // silahkan ditambahkan atribut lain jika diperlukan
}
