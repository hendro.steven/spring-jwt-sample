package com.training.spring.utils;

import org.springframework.util.StringUtils;

public class TokenExtractor {
    public static String extractToken(String header) {
        if (StringUtils.hasText(header) && header.startsWith("Bearer ")) {
            return header.substring(7);
        }
        return null;
    }
}
