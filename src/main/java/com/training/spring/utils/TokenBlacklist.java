package com.training.spring.utils;

public interface TokenBlacklist {

    void addToBlackList(String token);

    boolean isTokenBlackListed(String token);
}
