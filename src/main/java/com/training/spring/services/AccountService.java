package com.training.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.training.spring.models.entities.Accounts;
import com.training.spring.models.repos.AccountRepo;
import com.training.spring.models.repos.AccountRoleRepo;

@Service
public class AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private AccountRoleRepo accountRoleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Accounts getAccountByEmail(String email) {
        return accountRepo.findByEmail(email);
    }

    public String register(Accounts account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        if (account.getRoles().isEmpty()) {
            account.getRoles().add(accountRoleRepo.findByName("ROLE_USER"));
        }
        accountRepo.save(account);
        return "Account created successfully!";
    }
}
