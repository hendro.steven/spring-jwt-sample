package com.training.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.training.spring.models.entities.Accounts;
import com.training.spring.models.repos.AccountRepo;
import com.training.spring.utils.AccountDetails;

@Component
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Accounts account = accountRepo.findByEmail(username);
        if (account == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new AccountDetails(account);
    }

}
