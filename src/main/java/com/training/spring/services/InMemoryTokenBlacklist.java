package com.training.spring.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.training.spring.utils.TokenBlacklist;

@Service
public class InMemoryTokenBlacklist implements TokenBlacklist {

    private Set<String> blackList = new HashSet<>();

    @Override
    public void addToBlackList(String token) {
        blackList.add(token);
    }

    @Override
    public boolean isTokenBlackListed(String token) {
        return blackList.contains(token);
    }

}
