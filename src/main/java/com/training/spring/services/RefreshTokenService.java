package com.training.spring.services;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.spring.models.entities.RefreshToken;
import com.training.spring.models.repos.AccountRepo;
import com.training.spring.models.repos.RefreshTokenRepo;

@Service
public class RefreshTokenService {

    @Autowired
    private RefreshTokenRepo refreshTokenRepo;

    @Autowired
    private AccountRepo accountRepo;

    public RefreshToken createRefreshToken(String email) {

        RefreshToken refreshToken = RefreshToken.builder()
                .account(accountRepo.findByEmail(email))
                .expiryDate(Instant.now().plusMillis(600000))
                .token(UUID.randomUUID().toString())
                .build();
        return refreshTokenRepo.save(refreshToken);
    }

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepo.findByToken(token);
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepo.delete(token);
            throw new RuntimeException(token.getToken() + " Refresh token is expired. Please make a new login");
        }
        return token;
    }
}
