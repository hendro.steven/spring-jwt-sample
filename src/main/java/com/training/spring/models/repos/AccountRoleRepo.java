package com.training.spring.models.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.spring.models.entities.AccountRole;

public interface AccountRoleRepo extends JpaRepository<AccountRole, Long> {

    AccountRole findByName(String name);
}
