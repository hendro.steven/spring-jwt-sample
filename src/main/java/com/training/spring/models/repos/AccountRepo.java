package com.training.spring.models.repos;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.spring.models.entities.Accounts;

public interface AccountRepo extends JpaRepository<Accounts, UUID> {

    Accounts findByEmail(String email);
}
