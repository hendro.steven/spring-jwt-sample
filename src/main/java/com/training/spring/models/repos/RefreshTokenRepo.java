package com.training.spring.models.repos;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.training.spring.models.entities.RefreshToken;

public interface RefreshTokenRepo extends CrudRepository<RefreshToken, Long> {
    Optional<RefreshToken> findByToken(String token);

}
