package com.training.spring.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.spring.dto.AccountLoginRequest;
import com.training.spring.dto.AccountLoginResponse;
import com.training.spring.dto.AccountRequest;
import com.training.spring.dto.GenericResponse;
import com.training.spring.dto.RefreshTokenRequest;
import com.training.spring.models.entities.Accounts;
import com.training.spring.models.entities.RefreshToken;
import com.training.spring.services.AccountService;
import com.training.spring.services.InMemoryTokenBlacklist;
import com.training.spring.services.RefreshTokenService;
import com.training.spring.utils.JwtService;
import com.training.spring.utils.TokenExtractor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/auth")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private RefreshTokenService refreshTokenService;

    @Autowired
    private InMemoryTokenBlacklist tokenBlacklist;

    @PostMapping("/register")
    public ResponseEntity<GenericResponse<String>> addAccount(@RequestBody AccountRequest accountRequest) {
        GenericResponse<String> response = new GenericResponse<>();
        try {
            Accounts account = modelMapper.map(accountRequest, Accounts.class);
            accountService.register(account);
            response.setStatus(true);
            response.getMessages().add("Account created successfully");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<GenericResponse<AccountLoginResponse>> login(
            @RequestBody AccountLoginRequest accountRequest, HttpServletResponse httpResponse) {
        GenericResponse<AccountLoginResponse> response = new GenericResponse<>();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(accountRequest.getEmail(), accountRequest.getPassword()));

        if (authentication.isAuthenticated()) {
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(accountRequest.getEmail());
            String accessToken = jwtService.generateToken(accountRequest.getEmail());

            // create Cookie set accessToken
            ResponseCookie cookie = ResponseCookie.from("accessToken", accessToken)
                    .httpOnly(true)
                    .maxAge(1800) // 30 minutes
                    .path("/")
                    .build();
            httpResponse.addHeader(HttpHeaders.SET_COOKIE, cookie.toString());

            response.setStatus(true);
            response.getMessages().add("Login success");
            response.setPayloads(AccountLoginResponse.builder()
                    .accessToken(null)
                    .token(refreshToken.getToken()).build());
            return ResponseEntity.ok(response);
        } else {
            response.getMessages().add("Login failed");
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/refreshToken")
    public ResponseEntity<GenericResponse<AccountLoginResponse>> refreshToken(
            @RequestBody RefreshTokenRequest refreshTokenRequest) {

        GenericResponse<AccountLoginResponse> response = new GenericResponse<>();
        RefreshToken refreshToken = refreshTokenService.findByToken(refreshTokenRequest.getToken()).orElse(null);

        if (refreshToken == null) {
            response.getMessages().add("Invalid refresh token");
            return ResponseEntity.badRequest().body(response);
        }

        response.setStatus(true);
        response.getMessages().add("Token refreshed");
        response.setPayloads(AccountLoginResponse.builder()
                .accessToken(jwtService.generateToken(refreshToken.getAccount().getEmail()))
                .token(refreshToken.getToken()).build());

        return ResponseEntity.ok(response);
    }

    @PostMapping("/logout")
    public ResponseEntity<GenericResponse<String>> logout(HttpServletRequest request) {
        GenericResponse<String> response = new GenericResponse<>();

        String token = TokenExtractor.extractToken(request.getHeader("Authorization"));

        if (token == null) {
            response.getMessages().add("Invalid token");
            return ResponseEntity.badRequest().body(response);
        }

        tokenBlacklist.addToBlackList(token);
        response.setStatus(true);
        response.getMessages().add("Logout success");
        return ResponseEntity.ok(response);
    }
}
